/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package richtercloud.datanucleus.metamodel.bite.array.field;

import java.io.Serializable;
import java.util.ArrayList;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;

/**
 *
 * @author richter
 */
@Entity
public class MyEntity implements Serializable {
    @Id
    private Long id;
    @Basic
    @Lob
    private ArrayList<byte[]> imageData = new ArrayList<>();

    public MyEntity() {
    }

    public MyEntity(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ArrayList<byte[]> getImageData() {
        return imageData;
    }

    public void setImageData(ArrayList<byte[]> imageData) {
        this.imageData = imageData;
    }
}
